package com.agentd.tugasservice.repository;

import com.agentd.tugasservice.core.UserAgentD;

import java.util.HashMap;

public class UserRepository {
    HashMap<String, UserAgentD> repo;

    public UserRepository(){
        repo = new HashMap<>();
    }

    public HashMap<String,UserAgentD> getRepo(){
        return this.repo;
    }

    public void addUser(UserAgentD pengguna){
        repo.put(pengguna.getId(),pengguna);
    }

    public UserAgentD getUserById(String id){
        return repo.get(id);
    }
}
