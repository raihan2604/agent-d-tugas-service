package com.agentd.tugasservice.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class UserAgentD {
    String id;
    ArrayList<TugasIndividu> listTugasIndividu;
    static LogManager lgmngr = LogManager.getLogManager();
    static Logger log = lgmngr.getLogger(Logger.GLOBAL_LOGGER_NAME);


    public UserAgentD(String userID){
        id = userID;
        this.listTugasIndividu = new ArrayList<>();
    }

    void addTugasIndividu(TugasIndividu task){
        listTugasIndividu.add(task);
    }

    public String getId(){
        return this.id;
    }

    public void tambahTugasIndividu(String nama, String deskripsi, String deadline) {
        try {
            String[] deadlineSplit = deadline.split("-");
            String deadlineStruktur = deadlineSplit[0]+"/"+deadlineSplit[1]+"/"+deadlineSplit[2];
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date tanggal = dateFormat.parse(deadlineStruktur);
            TugasIndividu tugas = new TugasIndividu(nama, deskripsi, tanggal);
            this.addTugasIndividu(tugas);
        }catch (ParseException e){
            log.log(Level.INFO, "Error while parsing the date");
        }
    }

    public List<TugasIndividu> lihatTugasIndividu(){
        return this.listTugasIndividu;
    }
}
