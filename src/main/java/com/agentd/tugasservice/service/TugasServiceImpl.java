package com.agentd.tugasservice.service;


import com.agentd.tugasservice.core.TugasIndividu;
import com.agentd.tugasservice.core.UserAgentD;
import com.agentd.tugasservice.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TugasServiceImpl implements TugasService {

    private UserRepository repository = new UserRepository();

    @Override
    public UserAgentD findUser(String id) {
        return repository.getUserById(id);
    }

    @Override
    public void registerUser(UserAgentD pengguna) {
        repository.addUser(pengguna);
    }

    @Override
    public void registerTugas(String id,String nama, String desk, String deadline) {
        UserAgentD user = this.findUser(id);
        user.tambahTugasIndividu(nama,desk,deadline);
    }

    public UserRepository getRepository(){
        return this.repository;
    }

    @Override
    public List<TugasIndividu> findAllTugasOfUser(String id) {
        UserAgentD user = this.findUser(id);
        return user.lihatTugasIndividu();
    }

}
