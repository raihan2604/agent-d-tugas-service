package com.agentd.tugasservice.service;

import com.agentd.tugasservice.core.TugasIndividu;
import com.agentd.tugasservice.core.UserAgentD;

import java.util.List;

public interface TugasService {
    public UserAgentD findUser(String id);
    public void registerUser(UserAgentD pengguna);
    public void registerTugas(String id,String nama, String desk, String deadline);
    public List<TugasIndividu> findAllTugasOfUser(String id);

}
