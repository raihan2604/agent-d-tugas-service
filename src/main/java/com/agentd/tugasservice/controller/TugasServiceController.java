package com.agentd.tugasservice.controller;

import com.agentd.tugasservice.core.TugasIndividu;
import com.agentd.tugasservice.core.UserAgentD;
import com.agentd.tugasservice.service.TugasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/tugas")
public class TugasServiceController {

    @Autowired
    private TugasService service;


    @GetMapping("/find-user/{id}")
    public ResponseEntity<UserAgentD> findPenggunaById(@PathVariable String id){
        return new ResponseEntity<UserAgentD>(service.findUser(id), HttpStatus.OK);
    }

    @PostMapping("/register-user/{id}")
    public ResponseEntity registerPenggunaById(@PathVariable String id){
        service.registerUser(new UserAgentD(id));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/register-tugas/{id}/{nama}/{desk}/{deadline}")
    public ResponseEntity registerTugasUntukPengguna(
            @PathVariable String id,
            @PathVariable String nama,
            @PathVariable String desk,
            @PathVariable String deadline
    ){
        service.registerTugas(id,nama,desk,deadline);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/lihat-tugas/{id}")
    public ResponseEntity<List<TugasIndividu>> findAllTugasIndividu(@PathVariable String id){
        return new ResponseEntity<List<TugasIndividu>>(service.findAllTugasOfUser(id),HttpStatus.OK);
    }
}
