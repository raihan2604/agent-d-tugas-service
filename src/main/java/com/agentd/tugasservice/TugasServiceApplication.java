package com.agentd.tugasservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TugasServiceApplication.class, args);
    }

}
