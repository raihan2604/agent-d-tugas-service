package com.agentd.tugasservice.repository;

import com.agentd.tugasservice.core.UserAgentD;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserRepositoryTests {
    UserRepository repo;

    @BeforeEach
    public void setup(){
        this.repo = new UserRepository();
    }

    @Test
    public void testMethodGetRepo(){
        UserAgentD user = new UserAgentD("123abc");
        repo.addUser(user);
        assertEquals(1,repo.getRepo().size());
    }

    @Test
    public void testMethodAddUser(){
        UserAgentD user = new UserAgentD("123abc");
        repo.addUser(user);
        assertEquals(1,repo.getRepo().size());
    }

    @Test
    public void testMethodGetUserById(){
        UserAgentD user = new UserAgentD("123abc");
        repo.addUser(user);
        assertEquals("123abc",repo.getUserById("123abc").getId());
    }
}
